-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: farm
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animal`
--

DROP TABLE IF EXISTS `animal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barn_id` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `favorite_color` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmi1owfaomhqlw1gfbnfpca7fq` (`barn_id`),
  CONSTRAINT `FKmi1owfaomhqlw1gfbnfpca7fq` FOREIGN KEY (`barn_id`) REFERENCES `barn` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2733 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animal`
--

LOCK TABLES `animal` WRITE;
/*!40000 ALTER TABLE `animal` DISABLE KEYS */;
INSERT INTO `animal` VALUES (2700,2699,'Animal-0','2'),(2701,2699,'Animal-1','2'),(2702,2699,'Animal-2','2'),(2703,2699,'Animal-3','2'),(2704,2699,'Animal-4','2'),(2705,2699,'Animal-5','2'),(2706,2699,'Animal-6','2'),(2707,2699,'Animal-7','2'),(2708,2699,'Animal-8','2'),(2709,2699,'Animal-9','2'),(2710,2699,'Animal-10','2'),(2711,2699,'Animal-11','2'),(2712,2699,'Animal-12','2'),(2713,2699,'Animal-13','2'),(2714,2699,'Animal-14','2'),(2715,2699,'Animal-15','2'),(2716,2699,'Animal-16','2'),(2717,2699,'Animal-17','2'),(2718,2699,'Animal-18','2'),(2719,2699,'Animal-19','2'),(2721,2720,'Animal-20','0'),(2722,2720,'Animal-21','0'),(2723,2720,'Animal-22','0'),(2724,2720,'Animal-23','0'),(2725,2720,'Animal-24','0'),(2726,2720,'Animal-25','0'),(2727,2720,'Animal-26','0'),(2728,2720,'Animal-27','0'),(2729,2720,'Animal-28','0'),(2730,2720,'Animal-29','0'),(2732,NULL,'Animal-30','2');
/*!40000 ALTER TABLE `animal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barn`
--

DROP TABLE IF EXISTS `barn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `color` varchar(45) NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2732 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barn`
--

LOCK TABLES `barn` WRITE;
/*!40000 ALTER TABLE `barn` DISABLE KEYS */;
INSERT INTO `barn` VALUES (2699,'YELLOW-537170','2',20),(2720,'RED-452159','0',20),(2731,'YELLOW-853497','2',20);
/*!40000 ALTER TABLE `barn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (2733),(2733);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-12  8:06:40
