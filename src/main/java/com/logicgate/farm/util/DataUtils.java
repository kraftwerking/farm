package com.logicgate.farm.util;

import com.logicgate.farm.domain.Animal;
import com.logicgate.farm.domain.Color;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataUtils {

  public static List<Animal> getAnimalList() {
    List<Animal> animalList = IntStream.range(0, 20)
      .mapToObj(value -> new Animal(FarmUtils.animalName(value), Color.YELLOW))
      .collect(Collectors.toList());

    animalList.addAll(IntStream.range(0, 10)
      .mapToObj(value -> new Animal(FarmUtils.animalName(value), Color.RED))
      .collect(Collectors.toList()));

    animalList.addAll(IntStream.range(0, 5)
      .mapToObj(value -> new Animal(FarmUtils.animalName(value), Color.YELLOW))
      .collect(Collectors.toList()));

    return animalList;
  }

  public static List<Animal> getAnimalListTest2() {
    List<Animal> animalList = IntStream.range(0, 62)
      .mapToObj(value -> new Animal(FarmUtils.animalName(value), Color.YELLOW))
      .collect(Collectors.toList());

    return animalList;
  }
}
