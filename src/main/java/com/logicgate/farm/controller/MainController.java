package com.logicgate.farm.controller;

import com.logicgate.farm.domain.Animal;
import com.logicgate.farm.service.AnimalService;
import com.logicgate.farm.util.DataUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/")
public class MainController {

  @Autowired
  private AnimalService animalService;

  @GetMapping(path = "/test")
  public @ResponseBody
  String getTest() {
    List<Animal> newAnimals = DataUtils.getAnimalList();
    newAnimals.stream()
      .forEach(n -> animalService.addToFarm(n));

    List<Animal> animalsFromFarm = animalService.findAll();

    return "tests running...";
  }

  @GetMapping(path = "/test2")
  public @ResponseBody
  String getTest2() {
    List<Animal> newAnimals = DataUtils.getAnimalListTest2();
    animalService.addToFarm(newAnimals);

    List<Animal> animalsFromFarm = animalService.findAll();
    animalService.removeFromFarm(animalsFromFarm.get(0));
    animalService.removeFromFarm(animalsFromFarm.get(1));
    animalsFromFarm = animalService.findAll();

    return "tests running...";
  }
}
