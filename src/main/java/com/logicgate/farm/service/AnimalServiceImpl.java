package com.logicgate.farm.service;

import com.logicgate.farm.domain.Animal;
import com.logicgate.farm.domain.Barn;
import com.logicgate.farm.domain.Color;
import com.logicgate.farm.repository.AnimalRepository;
import com.logicgate.farm.repository.BarnRepository;
import com.logicgate.farm.util.FarmUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
@Transactional
public class AnimalServiceImpl implements AnimalService {

  private final AnimalRepository animalRepository;
  private final BarnRepository barnRepository;
  private List<Barn> currentBarns;
  private List<Animal> currentAnimals;

  @Autowired
  public AnimalServiceImpl(AnimalRepository animalRepository, BarnRepository barnRepository) {
    this.animalRepository = animalRepository;
    this.barnRepository = barnRepository;
    this.currentBarns = barnRepository.findAll();
    this.currentAnimals = animalRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Animal> findAll() {
    return animalRepository.findAll();
  }

  @Override
  public void deleteAll() {
    animalRepository.deleteAll();
  }

  @Override
  public Animal addToFarm(Animal animal) {
    // TODO: implementation of this method
    Barn targetBarn = getTargetBarn(animal.getFavoriteColor());
    animal.setBarn(targetBarn);
    animal = animalRepository.saveAndFlush(animal);
    currentAnimals = animalRepository.findAll();
    return animal;
  }

  public Barn getTargetBarn(Color color) {
    Map<Barn, List<Animal>> filteredAnimalMap = getAnimalsByBarnFilteredByColor(color);
    Barn target = new Barn(FarmUtils.barnName(color), color);

    // if no barn by color exists in barns, create new empty barn
    if (filteredAnimalMap.size() == 0) {
      target = barnRepository.saveAndFlush(target);
      currentBarns = barnRepository.findAll();
      return target;
    }

    // if exactly 1 barn by color exists in barns, and is not at capacity, return barn
    if (filteredAnimalMap.size() == 1) {
      target = filteredAnimalMap.keySet().iterator().next();
      List<Animal> animalList = filteredAnimalMap.get(target);
      if (animalList.size() < 20) return target;
    }

    // Keep in mind that when a new barn is added it will require a redistribution of animals in other barns.
    target = getTargetBarn(filteredAnimalMap);
    return target;
  }

  //TODO: can be replaced by custom query in jpa repository
  private Map<Barn, List<Animal>> getAnimalsByBarnFilteredByColor(Color color) {
    final Map<Barn, List<Animal>> animalsByBarn = currentAnimals.stream().collect(groupingBy(Animal::getBarn));

    return animalsByBarn.entrySet().stream()
      .filter(a -> a.getValue().stream()
        .anyMatch(aa -> aa.getFavoriteColor() == color)
      )
      .collect(Collectors.toMap(a -> a.getKey(), a -> a.getValue()));
  }

  private Barn getTargetBarn(Map<Barn, List<Animal>> filteredAnimalMap) {
    // evenly distribute X animals across X barns
    List<Barn> barnList = new ArrayList<>();
    List<Animal> animalList = new ArrayList<>();
    filteredAnimalMap.forEach((k, v) -> {
      barnList.add(k);
      animalList.addAll(v);
    });

    // if number of animals is divisible by num of barn we need to add a new barn then distribute animals across barns + 1 barns to accomodate new animal
    if ((animalList.size() % (barnList.size() * 20)) == 0) {
      Barn newBarn = barnRepository.saveAndFlush(new Barn(FarmUtils.barnName(animalList.get(0).getFavoriteColor()), animalList.get(0).getFavoriteColor()));
      currentBarns = barnRepository.findAll();
      barnList.add(newBarn);
    }

    redistributeAnimals(animalList, barnList);
    //update filtered map, get the barn w the fewest animals and return it
    filteredAnimalMap = getAnimalsByBarnFilteredByColor(animalList.get(0).getFavoriteColor());
    final Integer[] minBarn = {Integer.MAX_VALUE};
    final Barn[] target = {null};
    filteredAnimalMap.forEach((k, v) -> {
      if (target[0] == null || minBarn[0] > v.size()) {
        minBarn[0] = v.size();
        target[0] = k;
      }
    });

    return target[0];
  }

  private void redistributeAnimals(List<Animal> animalList, List<Barn> barnList) {
    final AtomicInteger counter = new AtomicInteger();

    final Collection<List<Animal>> partitionedAnimalList = animalList.stream()
      .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / barnList.size()))
      .values();

    partitionedAnimalList.stream().forEach(p -> {
      counter.set(0);
      p.stream().forEach(i -> {
        int n = counter.getAndIncrement();
        p.get(n).setBarn(barnList.get(n));
        animalRepository.saveAndFlush(p.get(n));
        currentAnimals = animalRepository.findAll();
      });
    });

  }

  @Override
  public void addToFarm(List<Animal> animals) {
    // TODO: implementation of this method
    animals.forEach(this::addToFarm);
  }

  @Override
  public void removeFromFarm(Animal animal) {
    // TODO: implementation of this method
    Color color = animal.getFavoriteColor();
    animal.setBarn(null);
    animalRepository.deleteById(animal.getId());
    animalRepository.flush();
    currentAnimals = animalRepository.findAll();

    // if we can remove a barn to minimize num of barns, delete 1 barn, resync the repository,
    // then distribute animals across barns - 1 barn
    Map<Barn, List<Animal>> filteredAnimalMap = getAnimalsByBarnFilteredByColor(color);

    List<Barn> barnList = new ArrayList<>();
    List<Animal> animalList = new ArrayList<>();
    filteredAnimalMap.forEach((k, v) -> {
      barnList.add(k);
      animalList.addAll(v);
    });

    if ((animalList.size() % ((barnList.size() - 1) * 20)) == 0) {
      animalList.stream()
        .filter(a -> a.getBarn().getId() == barnList.get(0).getId())
        .forEach(a -> {
          a.setBarn(null);
          a = animalRepository.saveAndFlush(a);
        });

      currentAnimals = animalRepository.findAll();
      barnRepository.deleteById(barnList.get(0).getId());
      barnRepository.flush();
      barnList.remove(0);
      currentBarns = barnRepository.findAll();
    }

    redistributeAnimals(animalList, barnList);
  }

  @Override
  public void removeFromFarm(List<Animal> animals) {
    animals.forEach(animal -> removeFromFarm(animalRepository.getOne(animal.getId())));
  }
}
